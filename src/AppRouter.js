import React from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import LandigPage from './component/LandingPage';
import PersonInfo from './component/PersonalInfo';
import IncomeInfo from './component/IncomeInfo';
import Error from './component/Error';

const AppRoute = () => (
  <Router>
      <Route path = "/" exact component={LandigPage} />
      <Route path = "/personalInfo" component={PersonInfo} />
      <Route path = "/income" component={IncomeInfo} />
      <Route path = "/" component={Error} />
  </Router>
)

export default AppRoute;