 
 const secIncomeReducer = (incomeInfo={ otherIncomes: [], primaryIncome:{} }, action) => {
    switch (action.type) {
        case 'UPDATE_PRIMARY_INCOME_SEC':
            return {...incomeInfo, primaryIncome: action.payload.primaryIncome};
        case 'ADD_OTHER_INCOME_SEC':
            incomeInfo.otherIncomes.push({ source: '', amount: '', frequency: '' });
            return {...incomeInfo };
        case 'REMOVE_OTHER_INCOME_SEC':
            incomeInfo.otherIncomes.splice(action.payload.index, 1);
            return {...incomeInfo };
        case 'UPDATE_OTHER_INCOME_SEC':
            const incomeToBeUpdated = incomeInfo.otherIncomes[action.payload.index];
            const updatedIncome = {...incomeToBeUpdated, ...action.payload.data }
            incomeInfo.otherIncomes[action.payload.index] = updatedIncome;
            return { ...incomeInfo };
        default:
            return incomeInfo;
    }
}

export default secIncomeReducer;