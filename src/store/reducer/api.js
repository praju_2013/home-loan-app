const apisReducer = (state = {}, action) => {
    switch (action.type) {
        case 'SUBMIT_APPLICATION':
            return { ...state, submitted: false, error: undefined, showLoading: true };
        case 'SUBMIT_APPLICATION_SUCCESS':
            return { ...state, showLoading: false, submitted: true, error: undefined };
        case 'SUBMIT_APPLICATION_ERROR':
            return { ...state, showLoading: false, submitted: false, error: action.payload.error };
        default:
            return state;
    }
}

export default apisReducer;