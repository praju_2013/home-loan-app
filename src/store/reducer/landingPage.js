
 
 const landingPageReducer = (landigPage={}, action) => {
    switch (action.type) {
        case 'SET_LANDING_PAGE_INFO':
            return {...landigPage, ...action.payload};
        default:
            return landigPage;
    }
}

export default landingPageReducer;