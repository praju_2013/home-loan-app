import address from './address'

const secPersonInfoReducer = (personInfo ={ address:{}}, action) => {
    switch(action.type){
        case 'UPDATE_PERSON_INFO_SEC':
            return { ...personInfo, ...action.payload };
        case 'UPDATE_ADDRESS_SEC':
            return { ...personInfo, address: address(personInfo.address, action) };
        default:
            return personInfo;
    }
}

export default secPersonInfoReducer;