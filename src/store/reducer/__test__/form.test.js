import formReducer from '../form';

describe('Form Reducer', () => {
    it('should return default updated form', () =>{
        const action = {type:'PERSONAL_INFO', payload: {incomeInfo: {otherIncomes:[], primaryIncome:{}}, personInfo:{}}};
        const updatedState = formReducer(undefined, action);
        expect(updatedState).toEqual({incomeInfo: {}, personInfo:{}});
    });
    it('should return validated personalinfo form', () =>{
        const form = {personInfo:{}, incomeInfo:{}}
        const action = {type:'VALIDATE_PERSONAL_INFO', payload: 
            {incomeInfo: {otherIncomes:[], primaryIncome:{}}, 
            personInfo:{firstName:'true', lastName:'true', birthday:'true', email:'true'}}};
        const updatedState = formReducer(form, action);
        expect(updatedState).toEqual({incomeInfo: {},  
            personInfo:{firstName:true, lastName:true, birthday:true, email:true}});
    });
    it('should return validated income info form', () =>{
        const form = {personInfo:{}, incomeInfo:{}}
        const action = {type:'VALIDATE_INCOME_INFO', payload: 
            {incomeInfo: {primIncome:[{source:'base salary', amount: '1000', frequency:'monthly'}], primaryIncome:{source:'base salary', amount: '1000', frequency:'monthly'}}, 
            personInfo:{}}};
        const updatedState = formReducer(form, action);
        expect(updatedState).toEqual({incomeInfo: {primIncome:true, primaryIncome:true },
            personInfo:{}});
    });
    
 
    
   
});