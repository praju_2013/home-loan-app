import addressReducer from '../address';

describe('Address reducer', () => {
    it('should return default state properly', () => {
        const action = {type: 'ADDRESS', payload: {city: 'xyz', suburb:'ou'}};
       const updatedState= addressReducer(undefined,action);
       expect(updatedState).toEqual({});
    });
    it('should return updated address properly', () => {
        const address = {unit:'3000'};
        const action = {type: 'UPDATE_ADDRESS', payload: {city: 'xyz', suburb:'ou'}};
       const updatedState= addressReducer(address, action);
       expect(updatedState).toEqual({unit:'3000', city: 'xyz', suburb:'ou'});
    });
    it('should return updated sec address properly', () => {
        const address = {unit:'3000'};
        const action = {type: 'UPDATE_ADDRESS_SEC', payload: {city: 'xyz', suburb:'ou'}};
       const updatedState= addressReducer(address, action);
       expect(updatedState).toEqual({unit:'3000', city: 'xyz', suburb:'ou'});
    });
});