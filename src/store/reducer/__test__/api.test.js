import apiReducer from '../api';

describe('API Reducer', () => {
    it ('should update default api correctly', () => {
        const action = {type:'APPICATION', payload: {}};
        const updateState = apiReducer(undefined, action);
        expect(updateState).toEqual({});
    });
    it ('should submit api correctly', () => {
        const application ={};
        const action = {type:'SUBMIT_APPLICATION', payload: {submitted: false, error: undefined, showLoading: true}}
        const updateState = apiReducer(application, action);
        expect(updateState).toEqual({submitted: false, error: undefined, showLoading: true});
    });
    it ('should update success api correctly', () => {
        const application = {};
        const action = {type:'SUBMIT_APPLICATION_SUCCESS', payload: {showLoading: false, submitted: true, error: undefined}}
        const updateState = apiReducer(application, action);
        expect(updateState).toEqual({showLoading: false, submitted: true, error: undefined});
    });
    it ('should update error api correctly', () => {
        const application = {};
        const action = {type:'SUBMIT_APPLICATION_ERROR', payload: {showLoading: false, submitted: false, error: 'error'}}
        const updateState = apiReducer(application, action);
        expect(updateState).toEqual({showLoading: false, submitted: false, error:'error'});
    });
});