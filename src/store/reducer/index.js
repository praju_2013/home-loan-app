import { combineReducers } from 'redux';
import personInfo from './personInfo';
import incomeInfo from './incomeInfo';
import landingPage from './landingPage';
import secPersonInfo from './secPersonInfo';
import secIncomeInfo from './secIncomeInfo';
import secForm from './secForm';
import form from './form';
import api from './api';

const rootReducer = combineReducers({ personInfo, incomeInfo, form, 
    landingPage, secPersonInfo, secIncomeInfo, secForm, api})

export default rootReducer;