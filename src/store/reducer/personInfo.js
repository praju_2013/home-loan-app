import address from './address'

const personInfoReducer = (personInfo ={ address:{}}, action) => {
    switch(action.type){
        case 'UPDATE_PERSON_INFO':
            return { ...personInfo, ...action.payload };
        case 'UPDATE_ADDRESS':
            return { ...personInfo, address: address(personInfo.address, action) };
        default:
            return personInfo;
    }
}

export default personInfoReducer;