import { validatePersonalInfo } from '../../component/Validation/personalInfoValid'
import { validateIncomeInfo } from '../../component/Validation/incomeInfoValid'

const secFormReducer = (form = { personInfo: {}, incomeInfo: {} }, action) => {
    switch (action.type) {
        case 'VALIDATE_PERSONAL_INFO_SEC':
            return { ...form, personInfo: validatePersonalInfo(action.payload) };
        case 'VALIDATE_INCOME_INFO_SEC':
            return { ...form, incomeInfo: validateIncomeInfo(action.payload) };
        default:
            return form;
    }
}



export default secFormReducer;