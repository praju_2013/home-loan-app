import { validatePersonalInfo } from '../../component/Validation/personalInfoValid'
import { validateIncomeInfo } from '../../component/Validation/incomeInfoValid'

const formReducer = (form = { personInfo:{}, incomeInfo:{} }, action) => {
    switch(action.type){
        case'VALIDATE_PERSONAL_INFO':
             return{...form, personInfo: validatePersonalInfo(action.payload)};
        case'VALIDATE_INCOME_INFO':
             return{...form, incomeInfo: validateIncomeInfo(action.payload)};
        default:
            return form;
    }
}



export default formReducer;