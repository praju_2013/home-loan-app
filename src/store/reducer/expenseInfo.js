
 
 const expenseReducer = (expenseInfo={}, action) => {
    switch (action.type) {
        case 'SET_EXPENSE_INFO':
            return {...expenseInfo, ...action.payload};
        default:
            return expenseInfo;
    }
}

export default expenseReducer;