const addressReducer = (address = {}, action) =>{
    switch (action.type) {
        case 'UPDATE_ADDRESS':
        case 'UPDATE_ADDRESS_SEC':
            return {...address, ...action.payload};
        default:
            return address;
    }
}
   
export default addressReducer;