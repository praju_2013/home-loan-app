 
 const incomeReducer = (incomeInfo={ otherIncomes: [], primaryIncome:{} }, action) => {
        switch (action.type) {
            case 'UPDATE_PRIMARY_INCOME':
                return {...incomeInfo, primaryIncome: action.payload.primaryIncome};
            case 'ADD_OTHER_INCOME':
                incomeInfo.otherIncomes.push({ source: '', amount: '', frequency: '' });
                return {...incomeInfo };
            case 'REMOVE_OTHER_INCOME':
                incomeInfo.otherIncomes.splice(action.payload.index, 1);
                return {...incomeInfo };
            case 'UPDATE_OTHER_INCOME':
                const incomeToBeUpdated = incomeInfo.otherIncomes[action.payload.index];
                const updatedIncome = {...incomeToBeUpdated, ...action.payload.data }
                incomeInfo.otherIncomes[action.payload.index] = updatedIncome;
                return { ...incomeInfo };
            default:
                return incomeInfo;
        }
 }

 export default incomeReducer;