import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import './App.css';
import Header from './component/Layout/Header';
import Content from './component/Layout/Content';



function App() {
  return (
    <React.Fragment>
      <Header />
      <Provider store={store}>
        <Content />
      </Provider>
      
    </React.Fragment>

);
}

export default App;
