import apiClient from '../apiClient'

export const submitHomeLoanApplication = () => {

    return (dispatch, getState) => {
        const state= getState();
        dispatch({ type: 'SUBMIT_APPLICATION' });

        const requestBody = {
            primaryApplicant: {
                personalDetails: { ...state.personInfo, dob: state.personInfo.birthday, birthday: undefined },
                incomeDetails: state.incomeInfo,
            },
            secondaryApplicant:{
                personalDetails: { ...state.secPersonInfo, dob: state.secPersonInfo.birthday, birthday: undefined },
                incomeDetails: state.secIncomeInfo,
            }
        };

        return apiClient.post('/submitApplication', requestBody)
            .then(data => {
                dispatch({ type: 'SUBMIT_APPLICATION_SUCCESS' });
            }).catch(error => {
                dispatch({ type: 'SUBMIT_APPLICATION_ERROR', payload: { error } });
            });
    };


}
