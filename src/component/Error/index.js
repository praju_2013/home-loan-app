import React from 'react';
import { connect } from 'react-redux';

const Error = ( { errorMessage }) => (<div className="container">
    <div className="text-danger">{errorMessage && errorMessage.stack}</div>
</div>);

const mapStoreToProps = store => ({
    errorMessage: store.api.error,
});

export default connect(mapStoreToProps)(Error);