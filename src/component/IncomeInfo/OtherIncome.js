import React from 'react';
import SelectInput from '../formElement/SelectInput';


/*{const OtherIncome = ({ otherIncomeInfo, error, setOtherIncomeInfo, index, value }) => (
    <SelectInput label="Secondary source of income" value={otherIncomeInfo.source}
    error={error} onChange={(val) => setOtherIncomeInfo({ index, data:{source: val} })}/>
);}*/

const OtherIncome = ({ error, otherIncomeInfo, setOtherIncomeInfo, index}) => (
    <SelectInput label="Primary source of income" value={otherIncomeInfo.source}
    error={error} onChange={(val) => setOtherIncomeInfo({index, data: { source: val }})}/>
);

export default OtherIncome;

/*
{incomeInfo.otherIncomes.map((otherIncome, index) => 
    <OtherIncome otherIncomeInfo={otherIncome} 
    setOtherIncome ={(index, data) => setIncomeInfo({})} index={index} />)}*/