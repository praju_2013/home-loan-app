import React from 'react';
import { shallow } from 'enzyme';
import IncomeItem from '../IncomeItem';

describe('IncomeItem', () => {
    it('should render correctly', () =>{
        const compo = shallow(<IncomeItem 
            incomeItem={{source: 'base salary', amount: '$100', frequency: 'annually' }} 
            setIncomeItem={jest.fn()} />)
        expect(compo).toMatchSnapshot();
    });
    it('should trigger onChange', () =>{
        const onChangeStub = jest.fn();
        const compo = shallow(<IncomeItem 
            incomeItem={{source: 'base salary', amount: '$100', frequency: 'annually' }} 
            setIncomeItem={onChangeStub} />)
        compo.find('SelectInput').simulate('change', { target: {value: 'salary'}});
        expect(onChangeStub).toBeCalled();
    });
    it('should trigger  handleOnChange', () =>{
        const handleOnChangeStub = jest.fn();
        const compo = shallow(<IncomeItem 
            incomeItem={{source: 'base salary', amount: '$100', frequency: 'annually' }} 
            setIncomeItem={handleOnChangeStub} />)
        //compo.find('IncomeInput').simulate('change', { target: {value: 'salary'}});
        compo.find('IncomeInput').props().handleOnChange({amount: '1000', frequency:'monthly'});
        expect(handleOnChangeStub).toBeCalled();
    });
});