import React from 'react';
import { shallow } from 'enzyme';
import { ApplicantIncomeView, mapStoreToProps, mapDispatchtoProps } from '../index';

describe('Applicant Income', () => {
    it('should render correctly 1 with showloading', () =>{
        const compo = shallow(<ApplicantIncomeView  primIncomeInfo={{}} 
            primForm={{}} secIncomeInfo={{}} secForm={{}}
            noOfApplicants='ONE' validateIncomeInfo={jest.fn()} 
            submitData={jest.fn()} showLoading={true} />)
        expect(compo).toMatchSnapshot()
    });
    it('should render correctly 1 without showloading', () =>{
        const compo = shallow(<ApplicantIncomeView  primIncomeInfo={{}} 
            primForm={{}} secIncomeInfo={{}} secForm={{}}
            noOfApplicants='ONE' validateIncomeInfo={jest.fn()} 
            submitData={jest.fn()} showLoading={false} />)
        expect(compo).toMatchSnapshot()
    });
    it('should render correctly 2 with showloading', () =>{
        const compo = shallow(<ApplicantIncomeView  primIncomeInfo={{}} 
            primForm={{}} secIncomeInfo={{}} secForm={{}}
            noOfApplicants='TWO' validateIncomeInfo={jest.fn()} 
            submitData={jest.fn()} showLoading={true} />)
        expect(compo).toMatchSnapshot()
    });
    it('should render correctly 2 without showloading', () =>{
        const compo = shallow(<ApplicantIncomeView  primIncomeInfo={{}} 
            primForm={{}} secIncomeInfo={{}} secForm={{}}
            noOfApplicants='TWO' validateIncomeInfo={jest.fn()} 
            submitData={jest.fn()} showLoading={true} />)
        expect(compo).toMatchSnapshot()
    });
    it('should trigger onClick for 2 applicants', () =>{
        const validateIncomeInfoStub = jest.fn();
        const submitDataStub =jest.fn();
        const compo = shallow(<ApplicantIncomeView  primIncomeInfo={{}} 
            primForm={{}} secIncomeInfo={{}} secForm={{}}
            noOfApplicants='TWO' validateIncomeInfo={validateIncomeInfoStub} 
            submitData={submitDataStub} showLoading={false} />);
        compo.find('button').at(1).simulate('click');
        expect(validateIncomeInfoStub).toBeCalled();
        expect(submitDataStub).toBeCalled();
    });
    it('should trigger onClick for 1 applicant', () =>{
        const validateIncomeInfoStub = jest.fn();
        const submitDataStub =jest.fn();
        const compo = shallow(<ApplicantIncomeView  primIncomeInfo={{}} 
            primForm={{}} secIncomeInfo={{}} secForm={{}}
            noOfApplicants='ONE' validateIncomeInfo={validateIncomeInfoStub} 
            submitData={submitDataStub} showLoading={false} />);
        compo.find('button').at(0).simulate('click');
        expect(validateIncomeInfoStub).toBeCalled();
        expect(submitDataStub).toBeCalled();
    });
    it('should map mapStoreToProps', () => {
        const store = { 
            landingPage:{
                noOfApplicants:'one'
            },
            incomeInfo:'dummy income info',
            secIncomeInfo:'dummy sec income info',
            form:{
                incomeInfo: 'form income info'
            },
            secForm:{
                incomeInfo: 'form sec income info'
            },
            api:{
                showLoading: true
            }

        };
        const mappedProps = mapStoreToProps(store);
        expect(mappedProps.noOfApplicants).toBe('one');
        expect(mappedProps.primIncomeInfo).toBe('dummy income info');
        expect(mappedProps.secIncomeInfo).toBe('dummy sec income info');
        expect(mappedProps.primForm).toBe('form income info');
        expect(mappedProps.secForm).toBe('form sec income info');
        expect(mappedProps.showLoading).toBe(true);
    });
    it('should map dispatchtoprops', () => {
        const dispatchStub = jest.fn();
        const mappedProps = mapDispatchtoProps(dispatchStub);
        mappedProps.validateIncomeInfo({}, true);
        expect(dispatchStub).toBeCalledWith( {
            type: 'VALIDATE_INCOME_INFO_SEC',
            payload: {}
        });
        mappedProps.validateIncomeInfo({}, false);
        expect(dispatchStub).toBeCalledWith( {
            type: 'VALIDATE_INCOME_INFO',
            payload: {}
        });
        mappedProps.submitData({});
        expect(dispatchStub).toBeCalledTimes(3);
    });
});