import React from 'react';
import {shallow } from 'enzyme';
import { IncomeInfoView as IncomeInfo , mapDispatchtoProps} from '../IncomeInfo';

describe('IncomeInfo', () =>{
    /*it('should render correctly', () => {
        const comp = shallow(<IncomeInfo incomeInfo={{ primaryIncome: {'salary'}, 
        otherIncomes:[{otherIncome:'base'}] }}
        updatePrimIncome = {jest.fn()}
        addOtherIncome ={jest.fn()} 
        updateOtherIncome ={jest.fn()} removeOtherIncome ={jest.fn()} />)
        expect(comp).toMatchSnapshot();
    });
    it('should render correctly', () => {
        const comp = shallow(<IncomeInfo incomeInfo={{ primaryIncome: 'salary', 
        otherIncomes:[{otherIncome:'base'}] }}
        updatePrimIncome = {jest.fn()}
        addOtherIncome ={jest.fn()} 
        updateOtherIncome ={jest.fn()} removeOtherIncome ={jest.fn()} />)
        expect(comp).toMatchSnapshot();
    }); */

    [
        {
            primaryIncome: {
                source: 'salary',
                amount: '1000',
                frequency: 'annually'
            },
            otherIncomes: [
                {
                    source: 'salary',
                    amount: '1000',
                    frequency: 'annually'
                },
                {
                    source: 'salary',
                    amount: '1000',
                    frequency: 'annually'
                },
                {
                    source: 'salary',
                    amount: '1000',
                    frequency: 'annually'
                }
            ],
        },

        {
            primaryIncome: {
                source: 'salary',
                amount: '1000',
                frequency: 'annually'
            },
            otherIncomes: [
                {
                    source: 'salary',
                    amount: '1000',
                    frequency: 'annually'
                },
                {
                    source: 'salary',
                    amount: '1000',
                    frequency: 'annually'
                }
            ],
        },

    ].forEach(incomeInfo => {
        it('should render correctly', () => {
            const comp = shallow(<IncomeInfo incomeInfo={incomeInfo}
            updatePrimIncome = {jest.fn()}
            addOtherIncome ={jest.fn()} 
            updateOtherIncome ={jest.fn()} removeOtherIncome ={jest.fn()} />)
            expect(comp).toMatchSnapshot();
        });
    });

    it('should mapDispatchToProps correctly', () => {
        const dispatchStub = jest.fn();
        const mappedProps = mapDispatchtoProps(dispatchStub);

        mappedProps.addOtherIncome(false);        
        expect(dispatchStub).toBeCalledWith({ type: 'ADD_OTHER_INCOME' });

        mappedProps.addOtherIncome(true);
        expect(dispatchStub).toBeCalledWith({ type: 'ADD_OTHER_INCOME_SEC' });

        mappedProps.updateOtherIncome(0, {}, true);
        expect(dispatchStub).toBeCalledWith({ type: 'UPDATE_OTHER_INCOME_SEC', payload: { index: 0, data: {}}});
        
        mappedProps.updateOtherIncome(0, {}, false);
        expect(dispatchStub).toBeCalledWith({ type: 'UPDATE_OTHER_INCOME', payload: { index: 0, data: {}}});

        mappedProps.updatePrimIncome({}, true);
        expect(dispatchStub).toBeCalledWith({ type: 'UPDATE_PRIMARY_INCOME_SEC', payload: { primaryIncome: {}}});

        mappedProps.updatePrimIncome({}, false);
        expect(dispatchStub).toBeCalledWith({ type: 'UPDATE_PRIMARY_INCOME', payload: { primaryIncome: {}}});

        mappedProps.removeOtherIncome(1, false);
        expect(dispatchStub).toBeCalledWith({ type: 'REMOVE_OTHER_INCOME', payload: { index: 1}});

        mappedProps.removeOtherIncome(1, true);
        expect(dispatchStub).toBeCalledWith({ type: 'REMOVE_OTHER_INCOME_SEC', payload: { index: 1}});
       
        expect(dispatchStub).toHaveBeenCalledTimes(8);
        dispatchStub.mockClear();

        mappedProps.validateIncomeInfo({}, true);
        expect(dispatchStub).toBeCalledWith({ type: 'VALIDATE_INCOME_INFO_SEC', payload: {}});
        
        mappedProps.validateIncomeInfo({}, false);
        expect(dispatchStub).toBeCalledWith({ type: 'VALIDATE_INCOME_INFO', payload: {}});
        
        expect(dispatchStub).toHaveBeenCalledTimes(2);
    });

    it('should trigger updatePrimIncome', () => {
        const updatePrimIncomeStub = jest.fn();
        const comp = shallow(<IncomeInfo incomeInfo={{primaryIncome: {}, otherIncomes: []}}
        updatePrimIncome = {updatePrimIncomeStub}
        addOtherIncome ={jest.fn()} 
        updateOtherIncome ={jest.fn()} removeOtherIncome ={jest.fn()} />)
        comp.find('IncomeItem').props().setIncomeItem({updatedPrimaryIncome: 'val'});
        expect(updatePrimIncomeStub).toBeCalled();
    });

    it('should trigger updateOtherIncome', () => {
        const updateOtherIncomeStub = jest.fn();
        const comp = shallow(<IncomeInfo 
            incomeInfo={{primaryIncome: {}, otherIncomes: [{}, {}]}}
            updatePrimIncome = {jest.fn()}
            addOtherIncome ={jest.fn()} 
            updateOtherIncome ={updateOtherIncomeStub} removeOtherIncome ={jest.fn()} />);
        expect(comp).toMatchSnapshot();
        comp.find('IncomeItem').at(1).props().setIncomeItem(1, {}, true);
        expect(updateOtherIncomeStub).toBeCalled();
    });

    it('should trigger addOtherIncome', () => {
        const addOtherIncomeStub = jest.fn();
        const comp = shallow(<IncomeInfo incomeInfo={{primaryIncome: {}, otherIncomes: []}}
        updatePrimIncome = {jest.fn()}
        addOtherIncome ={addOtherIncomeStub} 
        updateOtherIncome ={jest.fn()} removeOtherIncome ={jest.fn()} />)
        comp.find('FaButton').at(0).simulate('click')
        expect(addOtherIncomeStub).toBeCalled();
    });
    it('should trigger removeOtherIncome', () => {
        const removeOtherIncomeStub = jest.fn();
        const comp = shallow(<IncomeInfo incomeInfo={{primaryIncome: {}, otherIncomes: [{}]}}
        updatePrimIncome = {jest.fn()}
        addOtherIncome ={jest.fn()} 
        updateOtherIncome ={jest.fn()} removeOtherIncome ={removeOtherIncomeStub} />)
        comp.find('FaButton').at(0).simulate('click')
        expect(removeOtherIncomeStub).toBeCalled();
    });
});