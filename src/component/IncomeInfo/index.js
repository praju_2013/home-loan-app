import React from 'react';
import IncomeInfo from './IncomeInfo';
import Accordion from '../formElement/Accordion';
import { connect } from 'react-redux';
import { submitHomeLoanApplication } from '../../actions';

export const ApplicantIncomeView = ({ primIncomeInfo, primForm, secIncomeInfo, secForm,
    noOfApplicants, validateIncomeInfo, submitData, showLoading }) => {

    return (
        <div>
            {showLoading ? <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div> :
                (noOfApplicants === 'TWO' ?
                    <div>
                        <Accordion heading="Income of First Applicant">
                            <IncomeInfo incomeInfo={primIncomeInfo} incomeInfoInvalid={primForm} />
                            <div className="col-sm-12 mt-2">
                                <button className="btn" onClick={() => validateIncomeInfo(primIncomeInfo, false)}>
                                    Continue to Next Applicant</button>
                            </div>
                        </Accordion>

                        <Accordion heading="Income of Second Applicant">
                            <IncomeInfo incomeInfo={secIncomeInfo} incomeInfoInvalid={secForm} isSec />
                            <div className="col-sm-12 mt-2">
                                <button className="btn"
                                    onClick={() => {
                                        validateIncomeInfo(secIncomeInfo, true);
                                        submitData();
                                    }}>Submit</button>
                            </div>
                        </Accordion>

                    </div> :
                    <Accordion heading="Income of First Applicant">
                        <IncomeInfo incomeInfo={primIncomeInfo} incomeInfoInvalid={primForm} />
                        <div className=" row col-sm-12 mt-2 ">
                            <button className="btn" onClick={() => {
                                validateIncomeInfo(primIncomeInfo, false);
                                submitData();
                            }}>Submit</button>
                        </div>
                    </Accordion>)}

        </div>);
}

export const mapStoreToProps = (store) => ({
    noOfApplicants: store.landingPage.noOfApplicants,
    primIncomeInfo: store.incomeInfo,
    secIncomeInfo: store.secIncomeInfo,
    primForm: store.form.incomeInfo,
    secForm: store.secForm.incomeInfo,
    showLoading: store.api.showLoading
})

export const mapDispatchtoProps = (dispatch) => ({
    validateIncomeInfo: (incomeInfo, isSec) => {
        dispatch(
            {
                type: `VALIDATE_INCOME_INFO${isSec ? '_SEC' : ''}`,
                payload: incomeInfo
            })
    },
    submitData: (store) => {
        const functionTOBeDispatched = submitHomeLoanApplication(store);
        dispatch(functionTOBeDispatched);
    },
})
export default connect(mapStoreToProps, mapDispatchtoProps)(ApplicantIncomeView);
