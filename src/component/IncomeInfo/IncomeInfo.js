import React from 'react';
import { connect } from 'react-redux';
import FaButton from '../formElement/FaButton';
import IncomeItem from './IncomeItem';


export const IncomeInfoView = ({ incomeInfo, updatePrimIncome, 
    addOtherIncome, updateOtherIncome, removeOtherIncome, isSec }) => {
    return (
        <div className="row">
            <h4 className="col-sm-12"><b>Income Details</b></h4>
            <h5 className="col-sm-12">Primary Income Details </h5>

            <IncomeItem incomeItem={incomeInfo.primaryIncome}
                setIncomeItem={updatedPrimIncome => updatePrimIncome(updatedPrimIncome, isSec)} />
            <hr className="col-sm-12" />
            <h5 className="col-sm-12">Other Income Details </h5>

            {incomeInfo.otherIncomes.map((otherIncome, index) =>
                <React.Fragment key={index}>
                    <IncomeItem incomeItem={otherIncome}
                        setIncomeItem={otherIncome => updateOtherIncome(index, otherIncome, isSec)} />
                    <FaButton onClick={() => removeOtherIncome(index, isSec)} label="Remove" type="minus" /><br/>
                </React.Fragment>)}
           {incomeInfo.otherIncomes.length < 3 && 
            <FaButton onClick={() => addOtherIncome(isSec)} label="Add another income" type="plus" />} 
        
        </div>
    )
}

export const mapDispatchtoProps = (dispatch) => ({
    updatePrimIncome: (primaryIncome, isSec) => (dispatch({ type: `UPDATE_PRIMARY_INCOME${isSec ? '_SEC' : ''}`, payload: { primaryIncome } })),
    addOtherIncome: (isSec) => (dispatch({ type: `ADD_OTHER_INCOME${isSec ? '_SEC' : ''}` })),
    removeOtherIncome: (index, isSec) => (dispatch({ type: `REMOVE_OTHER_INCOME${isSec ? '_SEC' : ''}`, payload: { index } })),
    updateOtherIncome: (index, data, isSec) => (dispatch({ type: `UPDATE_OTHER_INCOME${isSec ? '_SEC' : ''}`, payload: { index, data } })),
    validateIncomeInfo: (incomeInfo, isSec) => (dispatch({
        type: `VALIDATE_INCOME_INFO${isSec ? '_SEC' : ''}`,
        payload: incomeInfo
    }))
})

export default connect(null, mapDispatchtoProps)(IncomeInfoView);