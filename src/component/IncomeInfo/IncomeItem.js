import React from 'react';
import SelectInput from '../formElement/SelectInput';
import IncomeInput from '../formElement/IncomeInput';

const IncomeItem = ({ incomeItem, setIncomeItem }) => (

    <React.Fragment>
        <SelectInput label="Source of income" value={incomeItem.source}
            options={[
                { label: 'Base Salary', value: 'base_salary' },
                { label: 'Permanent rental income', value: 'perm_rent' },
                { label: 'Vacational rental income', value: 'vac_rent' },
                { label: 'Allowance', value: 'allowance' },
                { label: 'Other', value: 'other' },
            ]}
            onChange={(val) => setIncomeItem({ ...incomeItem, source: val })} />

        <IncomeInput question="Before tax how much do you earn ?"
            value={{ amount: incomeItem.amount, frequency: incomeItem.frequency }}
            handleOnChange={(updatedAmtFreq) => setIncomeItem({ ...incomeItem, ...updatedAmtFreq})} />
    </React.Fragment>
);

export default IncomeItem;