import React from 'react'

const TextInput = ({ type, value, onChange, label, error}) =>(
    <div className="col-sm-8 mr-2 mb-2">
        <h2 className="badge">{label}</h2><br />
        <input type={type} value={value} className="form-control" 
            onChange={(e) => onChange(e.target.value)} />
        {error && <span className= "text-danger">valid field required{error}</span>}
    </div>
   
);


export default TextInput;