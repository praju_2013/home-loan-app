import React from 'react';
import PropTypes from 'prop-types';

const SelectInput = ({ label, value, options, onChange, placeholder, error }) => (
  
        <div className="col-sm-12 mb-3 mt-2">
            <h5 > {label} </h5>
            <div className="input-group">
            <div className="input-group-append">
                <select
                    className="form-control"
                    value={value} placeholder="Please select"
                    onChange={e => onChange(e.target.value)} >
                    <option value="">{placeholder}</option>
                    {options.map((option, index) => <option key={index} value={option.value}>{option.label}</option>)}
                </select>
            </div>
            </div>
       
        {error && <span className="text-danger" > {error} </span>}
    </div>);

export default SelectInput;

SelectInput.propTypes = {
    label: PropTypes.string,
    error: PropTypes.string,
    className: PropTypes.string,
    options: PropTypes.array,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
}

SelectInput.defaultProps = {
    label: '',
    className: '',
    error: '',
    value: '',
    placeholder: 'Select...',

    options: [],
}