import React, { useState } from 'react';

export const Accordion = ({ showByDefault= false, children, heading, disabled, showForced=false }) => {
    const [show, setShow] = useState(showByDefault);

    return (
        <div className="accordion row justify-content-center">
            <div className="card col-sm-12 col-md-6">
                <div className="card-header m-0" >
                    <h5 className="mb-0">
                        <button className="btn btn-link" type="button"
                            disabled={disabled}
                            onClick={() => setShow(!show)}
                            data-toggle="collapse">
                            {heading}
                         </button>
                    </h5>
                </div>

                <div className={`collapse ${show || showForced ? 'show' : ''}`}>
                    <div className="card-body">
                        {children}
                    </div>
                </div>
            </div>
        </div>
    );
}



export default Accordion;