import React from 'react';
import PropTypes from 'prop-types';


const IncomeInput = ({ question, value, frequencyList, handleOnChange, error }) => (
    <div className=" col-sm-12 mb-3 mt-2">
        <h5>{question} </h5>
        <div className="input-group">
            <div className="input-group-prepend">
                <span className="input-group-text">$</span>
            </div>
            <input value={value.amount} 
                onChange={e => handleOnChange({ ...value, amount: e.target.value })} 
                type="text" style={{ maxWidth: 200}} className="form-control" />
            <div className="input-group-append">
                <select value={value.frequency} className="form-control"
                    onChange={e => handleOnChange({ ...value, frequency: e.target.value })}>
                    <option value="">select</option>
                   {frequencyList.map((el, index) => <option key={index} value={el}>{el}</option>)}
                </select>
            </div>
        </div>
        {error && <span className="text-danger" > Field should not be empty </span>}
    </div>
);

IncomeInput.propTypes = {
    question: PropTypes.string,
    value: PropTypes.object.isRequired,
    frequencyList: PropTypes.array,
    handleOnChange: PropTypes.func.isRequired,
   // error: PropTypes.string,
}

IncomeInput.defaultProps = {
    value: { amount:'', frequency: ''},
    question: '',
  //  error: '',
    frequencyList: ['Annually', 'Monthly', 'Weekly'],
}

export default IncomeInput;
