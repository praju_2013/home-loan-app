import React from 'react';
import { shallow } from 'enzyme';
import SelectInput from '../SelectInput';

describe('Select Input', () => {
    it('should render correctly', () => {
        const compo = shallow(<SelectInput label='Primary Source' value='base salary' 
            options={[{label: 'X', value: 'x'}]} onChange={jest.fn()} placeholder='please select' 
            error='error occured' />)
        expect(compo).toMatchSnapshot();
    });
    it('should trigger onChange', () => {
        const onChangeStub = jest.fn();
        const compo = shallow(<SelectInput label='Primary Source' value='base salary' 
            options={[{label: 'X', value: 'x'}]} onChange={onChangeStub} placeholder='please select' 
            error='error occured' />)
        compo.find('select').simulate('change', {target: {value: 'val'}});
        expect(onChangeStub).toBeCalled();
    });
});