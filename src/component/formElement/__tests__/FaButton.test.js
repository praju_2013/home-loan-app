import React from 'react';
import { shallow } from 'enzyme';
import FaButton from '../FaButton';

describe('FaButton', () => {
    it('should render correctly with disabled', () => {
        const compo = shallow(<FaButton label='add' type = 'plus' size='md' 
            onClick={jest.fn()} disabled={true}/>)
        expect(compo).toMatchSnapshot();
    });
    it('should render correctly without disabled', () => {
        const compo = shallow(<FaButton label='add' type = 'plus' size='md' 
            onClick={jest.fn()} disabled={false}/>)
        expect(compo).toMatchSnapshot();
    });
    it('should trigger correctly', () => {
        const onClickStub = jest.fn();
        const compo = shallow(<FaButton label='add' onClick={onClickStub} />)
        compo.find('a').simulate('click');
        expect(onClickStub).toBeCalled();
    });
});