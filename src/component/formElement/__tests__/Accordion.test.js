import React, { useState } from 'react';
import { renderHook, act } from '@testing-library/react-hooks';
import { shallow } from 'enzyme';
import{ Accordion} from '../Accordion';
let container;
const HookWrapper = (props) => {
    const hook = props.hook ? props.hook() : undefined;
    return <div hook ={hook}/>
}

describe('Accordion', () => {
    it('should render correctly', () => {
        const comp = shallow(<Accordion showByDefault={ false} 
            children={{}} 
            heading='personal detail' disabled showForced={false} />)
            expect(comp).toMatchSnapshot();
    });
    /*test('should set val', () => {
       
        let showByDefault = false;
        const { result, rerender } = renderHook(() => useState(showByDefault));
        showByDefault = false;
        rerender()
        act(() => {
            button.simulate('click', {bubbles: true});
        })
      
        expect(result.current.show).toBe(false)
      })*/
    
});