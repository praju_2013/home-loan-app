import React from 'react';
import { shallow } from 'enzyme';
import RadioButton from '../RadioButton';

describe('Radio Input Button', () => {
    it('should render correctly', () => {
        const comp = shallow(<RadioButton question='plz select' name='noofapp' 
            radioOptions={[{label:'yes', value:'yes'}]} value='val' onChange={jest.fn()} />)
        expect(comp).toMatchSnapshot();
    });
    it('should trigger onChage', () => {
        const onChangeStub = jest.fn();
        const comp = shallow(<RadioButton question='plz select' name='noofapp' 
            radioOptions={[{label:'yes', value:'yes'}]} value='val' onChange={onChangeStub} />)
        comp.find('input').at(0).simulate('change', {target: {value: 'val'}});
        expect(onChangeStub).toBeCalled();
    });
});