import React from 'react';
import { shallow } from 'enzyme';
import TextInput from '../TextInput';

describe('TextInput', () => {
    it('verify textinput without error', () => {
        const comp = shallow(<TextInput type="text" label="input"
            value="input val" onChange={jest.fn()} />);
        expect(comp).toMatchSnapshot();
    });
    it('verify textinput with error', () => {
        const comp = shallow(<TextInput type="text" label="input" error="error occured"
            value="input val" onChange={jest.fn()} />);
        expect(comp).toMatchSnapshot();
    });
    it('should trigger onchange', () => {
        const onChangeStub = jest.fn();
        const comp = shallow(<TextInput type="text" label="input" error="error occured"
            value="input val" onChange={onChangeStub} />);
        comp.find('input').simulate('change', { target: { value: 'test dummy' } });
        expect(onChangeStub).toBeCalled();
    });
});
