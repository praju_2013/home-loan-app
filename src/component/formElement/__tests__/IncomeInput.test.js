import React from 'react';
import { shallow } from 'enzyme';
import IncomeInput from '../IncomeInput';

describe('income input', () => {
    it('should render correctly', () => {
        const compo = shallow(<IncomeInput question='no of app' value='val' 
            frequencyList={[]} handleOnChange={jest.fn()} error='error occured'/>)
        expect(compo).toMatchSnapshot();
    });
    it('should trigger onchange', () => {
        const handleOnChangeStub = jest.fn();
        const compo = shallow(<IncomeInput question='no of app' value='val' 
            frequencyList={[]} handleOnChange={handleOnChangeStub} error='error occured'/>)
        compo.find('input').simulate('change', {target: {value: '1000'}});
        expect(handleOnChangeStub).toBeCalled();
    });
    it('should trigger onchange', () => {
        const handleOnChangeStub = jest.fn();
        const compo = shallow(<IncomeInput question='no of app' value='val' 
            frequencyList={['annually', 'monthly']} handleOnChange={handleOnChangeStub} error='error occured'/>)
        compo.find('select').simulate('change', {target: {value: 'annually'}});
        expect(handleOnChangeStub).toBeCalled();
    });
});