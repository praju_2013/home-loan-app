import React from 'react';
import PropTypes from 'prop-types';

const RadioButton = ({ question, name, radioOptions, value, onChange }) => (
    <div>
        <h6>  {question} </h6>
        {radioOptions.map((option, index) => (
            <React.Fragment key={index}>
                <input type="radio" name={name}
                    checked={option.value === value}
                    onChange={() => onChange(option.value)} />
                {option.label}<br/>
            </React.Fragment>
        )
        )}
    </div>
);

RadioButton.propTypes = {
    question: PropTypes.string,
    name: PropTypes.string.isRequired,
    radioOptions: PropTypes.array,
}

RadioButton.defaultProps = {
    question: '',
    radioOptions: [],
}

export default RadioButton;