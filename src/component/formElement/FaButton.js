/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';

const FaButton = ({ label, type = '', size='md', onClick, disabled=false }) => (
    <div className=" col-sm-12 mt-2">
        <a href="#" onClick={onClick} className={`btn btn-info btn-${size} ${disabled ? 'disabled' : ''}`} >
            <span className={`fa fa-${type}`}></span>
            <span> {label} </span>
        </a>
    </div>
);

export default FaButton;