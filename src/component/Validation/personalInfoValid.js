import { isEmpty, invalidChars, invalidEmail, isFutureDate } from './rules'
     
export const validatePersonalInfo = (personInfo) => {
  const invalid = {
      firstName: isEmpty(personInfo.firstName) || invalidChars(personInfo.firstName),
      lastName: isEmpty(personInfo.lastName) || invalidChars(personInfo.lastName),
      birthday: isEmpty(personInfo.birthday)||isFutureDate (personInfo.birthday),
      email: isEmpty(personInfo.email) || invalidEmail(personInfo.email)
  }
  const valid = Object.keys(invalid).filter(key => invalid[key] === true).length === 0 ;
  if(valid){
    invalid.valid = true;
  }
  return invalid;
}
