/* eslint-disable no-useless-escape */
export const isEmpty = (value) => (!value || value === '') ? true : false;
export const charatersOnly = (value) => /^[a-zA-Z]+$/.test(value);
export const invalidChars = value => !charatersOnly(value);
export const invalidEmail = (value) => {
   const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   return !(re.test(String(value).toLowerCase()));
}


export const isFutureDate = inputDate => {
    const d1 = new Date();
    const d2 = new Date(inputDate);
    return d1.getTime() < d2.getTime();
}

export const isBelow18 = birthday => {
    var ageDifMs = Date.now() - new Date(birthday).getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970) < 18;
}
