import { isEmpty } from './rules'

export const validateIncomeInfo = (incomeInfo) => {
    const invalid = {
        primaryIncome: isEmpty(incomeInfo.primaryIncome),
        primIncome: isEmpty(incomeInfo.primIncome)
    }
    const valid = Object.keys(invalid).filter(key => invalid[key] === true).length === 0 ;
    if(valid){
        invalid.valid = true;
    }
    return invalid;
}

