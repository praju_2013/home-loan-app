import React from 'react';
import { connect } from 'react-redux';
import Address from './address'
import TextInput from '../formElement/TextInput';

export const PersonInfoView = ({ personInfo, updatePersonInfo, setAddress,  personInfoInvalid, isSec }) => {
    /*useEffect(() => {
        if (personInfoInvalid && personInfoInvalid.valid) {
            history.push('/income');
        }
    });*/

    return (
        <div>
            <div className="row">
                <h4 className="col-sm-12"><b>Personal Details</b></h4>

                <TextInput error={personInfoInvalid.firstName}
                    type="text" label="First Name" value={personInfo.firstName}
                    onChange={(value) => updatePersonInfo({ firstName: value }, isSec)} />

                <TextInput error={personInfoInvalid.lastName}
                    type="text" label="Last Name" value={personInfo.lastName} required
                    onChange={(value) => updatePersonInfo({ lastName: value }, isSec)} />

                <TextInput error={personInfoInvalid.birthday}
                    type="date" label="DOB" value={personInfo.birthday} required
                    onChange={(value) => updatePersonInfo({ birthday: value }, isSec)} />

                <TextInput type="tel" label="Mobile Number" value={personInfo.phone} required
                    onChange={(value) => updatePersonInfo({ phone: value }, isSec)} />

                <TextInput error={personInfoInvalid.email}
                    type="text" label="Email Id" value={personInfo.email} required
                    onChange={(value) => updatePersonInfo({ email: value }, isSec)} />
            </div>
                <h5 className="mt-2"> <b>Address</b></h5>
                <div className="row">
                    <Address address={personInfo.address} setAddress={(val) => setAddress(val, isSec)} />

                </div>
        </div>
    );
}


export const mapDispatchtoProps = (dispatch) => ({
    updatePersonInfo: (updatedData, isSec) => (dispatch({
        type: `UPDATE_PERSON_INFO${isSec ? '_SEC' : ''}`,
        payload: updatedData
    })),
    setAddress: (updatedData, isSec) => (dispatch({
        type: `UPDATE_ADDRESS${isSec ? '_SEC' : ''}`,
        payload: updatedData
    })),
    validatePersonalInfo: (personInfo, isSec) => (dispatch({
        type: `VALIDATE_PERSONAL_INFO${isSec ? '_SEC' : ''}`,
        payload: personInfo
    }))
})
export default connect(null, mapDispatchtoProps)(PersonInfoView);
