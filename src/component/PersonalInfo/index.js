import React,{ useEffect } from 'react';
import PersonInfo from './PersonInfo';
import { connect } from 'react-redux';
import Accordion from '../formElement/Accordion';

export const ApplicantView = ({ noOfApplicants, primPersonInfo, secPersonInfo, primForm,
    secForm, validatePersonalInfo, history }) => {
        useEffect(() => {
            if((noOfApplicants==='TWO' && secForm.valid)
                ||(noOfApplicants==='ONE' && primForm.valid)){
                history.push('income')
            }
        })
    return (
        <div>
            {noOfApplicants === 'TWO' ?
                <div>
                    <Accordion heading="Applicant One" showForced={!primForm.valid}>
                        <PersonInfo personInfo={primPersonInfo} personInfoInvalid={primForm} />
                        <div className="col-sm-12 mt-2">
                            <button className="btn" 
                                onClick={() => validatePersonalInfo(primPersonInfo, false)}
                            > Continue to next Applicant</button>
                        </div>
                    </Accordion>
                    <Accordion heading="Applicant Two" 
                        disabled={!primForm.valid} showForced={primForm.valid}>
                        <PersonInfo personInfo={secPersonInfo} personInfoInvalid={secForm} isSec />
                        <div className="col-sm-12 mt-2">
                            <button className="btn" 
                                onClick={() => validatePersonalInfo(secPersonInfo, true)}>
                                Next</button>
                        </div>
                    </Accordion>
                </div> :
                <Accordion heading="Applicant Details">
                    <PersonInfo personInfo={primPersonInfo} personInfoInvalid={primForm} />
                    <div className="col-sm-12 mt-2">
                        <button className="btn" 
                            onClick={() => validatePersonalInfo(primPersonInfo, false)}>
                        Next</button>
                    </div>
                </Accordion>}
        </div>
    )
};

const mapStoreToProps = (store) => ({
    noOfApplicants: store.landingPage.noOfApplicants,
    primPersonInfo: store.personInfo,
    secPersonInfo: store.secPersonInfo,
    primForm: store.form.personInfo,
    secForm: store.secForm.personInfo
});

const mapDispatchtoProps = (dispatch) => ({
    validatePersonalInfo: (personInfo, isSec) => (dispatch(
        {
            type: `VALIDATE_PERSONAL_INFO${isSec ? '_SEC' : ''}`,
            payload: personInfo
        }))
})

export default connect(mapStoreToProps, mapDispatchtoProps)(ApplicantView);
