import React from 'react';
import TextInput from '../formElement/TextInput'

const Address = ({ address, setAddress }) => (
    <React.Fragment>
        <TextInput type="text" label="Unit and Street" value={address.unitandstreet}
            onChange={(e) => setAddress({ unitandstreet: e })} />

        <TextInput type="text" label="Suburb" value={address.Suburb}
            onChange={(e) => setAddress({ Suburb: e })} />

        <TextInput type="text" label="City" value={address.City}
            onChange={(e) => setAddress({ City: e })} />

        <TextInput type="number" label="Postcode" value={address.Postcode}
            onChange={(e) => setAddress({ Postcode: e })} />

        <TextInput type="text" label="State" value={address.State}
            onChange={(e) => setAddress({ State: e })} />
    </React.Fragment>

);



export default Address;