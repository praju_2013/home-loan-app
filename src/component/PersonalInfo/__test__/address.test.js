import React from 'react';
import { shallow } from 'enzyme';
import Address from '../address';

describe('Address', () => {
    it('should render correctly', () => {
        const compo = shallow(<Address address={{}} setAddress={jest.fn()} />)
        expect(compo).toMatchSnapshot();
    });
    it('should trigger onChange', () => {
        const setAddressStub = jest.fn();
        const compo = shallow(<Address address={{}} setAddress={setAddressStub} />);
        expect(compo).toMatchSnapshot();
        compo.find('TextInput').at(0).simulate('change', {target: {value: 'city'}})
        expect(setAddressStub).toBeCalled();
    });
    it('should trigger onChange', () => {
        const setAddressStub = jest.fn();
        const compo = shallow(<Address address={{}} setAddress={setAddressStub} />);
        expect(compo).toMatchSnapshot();
        compo.find('TextInput').at(1).simulate('change', {target: {value: 'city'}})
        expect(setAddressStub).toBeCalled();
    });
    it('should trigger onChange', () => {
        const setAddressStub = jest.fn();
        const compo = shallow(<Address address={{}} setAddress={setAddressStub} />);
        expect(compo).toMatchSnapshot();
        compo.find('TextInput').at(2).simulate('change', {target: {value: 'city'}})
        expect(setAddressStub).toBeCalled();
    });
    it('should trigger onChange', () => {
        const setAddressStub = jest.fn();
        const compo = shallow(<Address address={{}} setAddress={setAddressStub} />);
        expect(compo).toMatchSnapshot();
        compo.find('TextInput').at(3).simulate('change', {target: {value: 'city'}})
        expect(setAddressStub).toBeCalled();
    });
    it('should trigger onChange', () => {
        const setAddressStub = jest.fn();
        const compo = shallow(<Address address={{}} setAddress={setAddressStub} />);
        expect(compo).toMatchSnapshot();
        compo.find('TextInput').at(4).simulate('change', {target: {value: 'city'}})
        expect(setAddressStub).toBeCalled();
    });
});