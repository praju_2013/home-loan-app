import React from 'react';
import {shallow } from 'enzyme';
import { PersonInfoView, mapDispatchtoProps } from '../PersonInfo';

describe('Personal Info', () => {
    it('should render correctly', () => {
        const compo = shallow(<PersonInfoView  personInfo={{}} 
            updatePersonInfo={jest.fn()} setAddress={jest.fn()}  personInfoInvalid={{}} isSec/>);
        expect(compo).toMatchSnapshot();
    });
    it('should trigger onchange', () => {
        const updatePersonInfoStub = jest.fn();
        const compo = shallow(<PersonInfoView  personInfo={{}} 
            updatePersonInfo={updatePersonInfoStub} setAddress={jest.fn()}  personInfoInvalid={{}} isSec/>);
        compo.find('TextInput').at(0).simulate('change', {target: {value: 'firstname'}})
        expect(updatePersonInfoStub).toBeCalled();
    });
    it('should trigger onchange', () => {
        const updatePersonInfoStub = jest.fn();
        const compo = shallow(<PersonInfoView  personInfo={{}} 
            updatePersonInfo={updatePersonInfoStub} setAddress={jest.fn()}  personInfoInvalid={{}} isSec/>);
        compo.find('TextInput').at(1).simulate('change', {target: {value: 'firstname'}})
        expect(updatePersonInfoStub).toBeCalled();
    });
    it('should trigger onchange', () => {
        const updatePersonInfoStub = jest.fn();
        const compo = shallow(<PersonInfoView  personInfo={{}} 
            updatePersonInfo={updatePersonInfoStub} setAddress={jest.fn()}  personInfoInvalid={{}} isSec/>);
        compo.find('TextInput').at(2).simulate('change', {target: {value: 'firstname'}})
        expect(updatePersonInfoStub).toBeCalled();
    });
    it('should trigger onchange', () => {
        const updatePersonInfoStub = jest.fn();
        const compo = shallow(<PersonInfoView  personInfo={{}} 
            updatePersonInfo={updatePersonInfoStub} setAddress={jest.fn()}  personInfoInvalid={{}} isSec/>);
        compo.find('TextInput').at(3).simulate('change', {target: {value: 'firstname'}})
        expect(updatePersonInfoStub).toBeCalled();
    });
    it('should trigger onchange', () => {
        const updatePersonInfoStub = jest.fn();
        const compo = shallow(<PersonInfoView  personInfo={{}} 
            updatePersonInfo={updatePersonInfoStub} setAddress={jest.fn()}  personInfoInvalid={{}} isSec/>);
        compo.find('TextInput').at(4).simulate('change', {target: {value: 'firstname'}})
        expect(updatePersonInfoStub).toBeCalled();
    });
    it('should trigger onchange', () => {
        const setAddressStub = jest.fn();
        const compo = shallow(<PersonInfoView  personInfo={{}} 
            updatePersonInfo={jest.fn()} setAddress={setAddressStub}  personInfoInvalid={{}} isSec/>);
            compo.find('Address').props().setAddress();
        expect(setAddressStub).toBeCalled();
    });
    it('should map mapDispatchToProps', () => {
        const dispatchStub = jest.fn();
        const mappedProps = mapDispatchtoProps(dispatchStub);

        mappedProps.updatePersonInfo({}, true);
        expect(dispatchStub).toBeCalledWith({ type: 'UPDATE_PERSON_INFO_SEC', payload:{} });
        mappedProps.updatePersonInfo({}, false);
        expect(dispatchStub).toBeCalledWith({ type: 'UPDATE_PERSON_INFO', payload:{} });

        mappedProps.setAddress({}, true);
        expect(dispatchStub).toBeCalledWith({type: 'UPDATE_ADDRESS_SEC', payload:{} });
        mappedProps.setAddress({}, false);
        expect(dispatchStub).toBeCalledWith({type: 'UPDATE_ADDRESS', payload:{} });

        mappedProps.validatePersonalInfo({}, true);
        expect(dispatchStub).toBeCalledWith({type: 'VALIDATE_PERSONAL_INFO_SEC', payload:{} });
        mappedProps.validatePersonalInfo({}, false);
        expect(dispatchStub).toBeCalledWith({type: 'VALIDATE_PERSONAL_INFO', payload:{} });
    });
});