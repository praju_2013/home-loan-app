import React from 'react';
import { shallow } from 'enzyme';
import {ApplicantView} from '../index';

describe('Applicant Info', () =>{
    it('should render correctly', () => {
        const compo = shallow(<ApplicantView noOfApplicants='one' primPersonInfo={{}} secPersonInfo={{}} primForm={{}}
            secForm={{}} validatePersonalInfo={{}} history={{}} />)
        expect(compo).toMatchSnapshot();        
    });
});