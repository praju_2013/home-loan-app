import React from 'react';
import { connect } from 'react-redux';


const ExpenseInfo = ({ expenseInfo, setExpenseInfo }) => (
<div>
    <h1>Expense Details</h1>
</div>
);


const mapStoretoProps = (store) => ({
    expenseInfo: store.expenseInfo,

})

const mapDispatchtoProps = (dispatch) => ({
    setExpenseInfo: updatedData => (dispatch({ type: 'SET_EXPENSE_INFO', payload: updatedData })),
    
})

export default connect(mapStoretoProps, mapDispatchtoProps)(ExpenseInfo);