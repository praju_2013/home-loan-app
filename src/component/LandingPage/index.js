import React from 'react';
import { connect } from 'react-redux';
import RadioButton from '../formElement/RadioButton';
import FaButton from '../formElement/FaButton'

export const LandingPageView = ({ noOfApplicants, setLandingPage, history }) => (
    <React.Fragment>
        <RadioButton
            question=" Please select number of Applicants"
            name="noofapplicants"
            value={noOfApplicants}
            radioOptions={[{ label: 'One', value: 'ONE' }, { label: 'Two', value: 'TWO' }]}
            onChange={(updatedVal) => setLandingPage({ noOfApplicants: updatedVal })} />

        <FaButton type="chevron-circle-right" onClick={() => history.push('/personalInfo')} label="next"
            disabled={!noOfApplicants} />


    </React.Fragment>

);

export const mapStoreToProps = (store) => ({
    noOfApplicants: store.landingPage.noOfApplicants
});

export const mapDispatchToProps = (dispatch) => ({
    setLandingPage: updatedData => dispatch({ type: 'SET_LANDING_PAGE_INFO', payload: updatedData })
})
export default connect(mapStoreToProps, mapDispatchToProps)(LandingPageView);
