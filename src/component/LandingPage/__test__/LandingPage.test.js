import React from 'react';
import { shallow } from 'enzyme';
import{ LandingPageView, mapStoreToProps, mapDispatchToProps }from '../index';

describe('Landing Page', () => {
    it('should render correctly', () => {
        const comp = shallow(<LandingPageView  noOfApplicants='ONE' 
            setLandingPage={jest.fn()} history={{push: jest.fn()}} />);
        expect(comp).toMatchSnapshot();
    });
    it('should trigger correctly', () => {
        const setLandingPageStub = jest.fn();
        const comp = shallow(<LandingPageView  noOfApplicants='ONE' 
            setLandingPage={setLandingPageStub} history={{push: jest.fn()}} />);
        comp.find('RadioButton').simulate('change');
        comp.find('FaButton').simulate('click');
        expect(setLandingPageStub).toBeCalled();
    });
    it('should map mapStoreToProps', () => {
        const store = {
            landingPage:{
                noOfApplicants: 'One'
            }
        }
        const mappedProps = mapStoreToProps(store);
        expect(mappedProps.noOfApplicants).toBe('One');
    });
    it('should map mapDispatchToProps', () => {
        const dispatchStub = jest.fn();
        const mappedProps = mapDispatchToProps(dispatchStub);
        mappedProps.setLandingPage({});
        expect(dispatchStub).toBeCalledWith({
            type: 'SET_LANDING_PAGE_INFO', 
            payload:{}
        });
    });
});