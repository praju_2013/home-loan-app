import React from 'react';
import AppRouter from '../../AppRouter';


const Content = () => (
    <div className="container">
        <AppRouter/>
    </div>);

export default Content;